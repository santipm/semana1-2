<!DOCTYPE html>
<html lang="es">
<head>
	<title>Santiago Ponce - Curso Wordpress</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="estilos.css" />
	<link rel="shortcut icon" href="/favicon.ico" />
</head>
 
<body>
<?php

	// Tabla de la verdad
	// V & V = V
	// V & F = F
	// F & V = F
	// F & F = F
	
	// V OR V = V
	// V OR F = V
	// F OR V = V
	// F OR F = F
	
	$bandera = TRUE;
	$hayProductos = FALSE;
	
	if($bandera && $hayProductos){
		echo "Se cumple la condición";
	}else{
		echo "NO cumple la condición";
	}
?>
</body>