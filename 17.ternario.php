<?php
//Operadores ternario
$mes =2;
if($mes === 2){
	echo ' mes de Febrero';
}else{
	echo ' No es mes de Febrero';
}

$mensaje = ($mes===2) ? 'mes de Febrero' : 'No es mes de Febrero';

//Más simplificado


echo ($mes===2) ? 'mes de Febrero' : 'No es mes de Febrero';

?>