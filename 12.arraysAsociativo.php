<?php
//Crear un Array
$valores = array(
					"nombre"    => "Santiago",
					"apellidos" =>	"Ponce",
					"edad"		=>	34,
				);

//Insertar Datos en un Array
	$valores["trabaja"] = TRUE;
	echo "<pre>";
	//print_r($valores);
	echo "</pre>";
	
//Editar
	$valores["trabaja"] = FALSE;
	echo "<pre>";
	//print_r($valores);
	echo "</pre>";

//Borrar
	unset($valores["trabaja"]);
	echo "<pre>";
	//print_r($valores);
	echo "</pre>";
//Mostrar un dato
//echo $valores["nombre"];

/*Array donde cada elemento es otro Array*/
$usuarios = array();

$usuario1 = array(
				"nombre"	=>	"Santiago",
				"trabaja"	=>	TRUE
			);
			
$usuario2 = array(
				"nombre"	=>	"José Antonio",
				"trabaja"	=>	TRUE
			);



array_push($usuarios,$usuario1,$usuario2);

echo "<pre>";
print_r($usuarios);
echo "</pre>";

echo $usuarios[1]["nombre"];
?>