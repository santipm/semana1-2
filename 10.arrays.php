<?php
//Crear un Array
$valores = array();

//Insertar Datos en un Array
$valores[] = "Primer Elemento";
array_push($valores,"Segundo Elemento", 3);
array_unshift($valores,"Soy ahora el primero");

//Mostar todo el Array
//var_dump($valores);
//echo '</br>';
//echo '</br>';
//print_r($valores);

//Ver un valor en concreto
echo 'Valor en concreto </br>';
//echo $valores[1];
//Editar un valor 
echo 'Editar un valor </br>';
$valores[1] = "Ya no soy el primero";
//echo $valores[1];
//Eliminar array y un valor en concreto

$clave = array("limón", "naranja", "banana", "albaricoque");
var_dump($clave); 
sort($clave);
echo '</br>';
var_dump($clave);
?>